terraform {
  backend "pg" {
    schema_name = "max-data"
    conn_str    = "postgres://terraform@10.42.0.1/terraform_state"
  }

  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "libvirt" {
  uri = "qemu+ssh://root@atlas.lan/system"
}

resource "libvirt_volume" "data" {
  name = "max-data"
  pool = "data"
  size = 1024 * 1024 * 1024 * 65

  lifecycle {
    prevent_destroy = true
  }
}

output "data_disk_id" {
  value = libvirt_volume.data.id
}
