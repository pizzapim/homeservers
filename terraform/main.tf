terraform {
  backend "pg" {
    schema_name = "max"
    conn_str    = "postgres://terraform@10.42.0.1/terraform_state"
  }

  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "libvirt" {
  uri = "qemu+ssh://root@atlas.lan/system"
}

module "tf-datatest" {
  source            = "git::https://git.pim.kunis.nl/home/tf-modules.git//debian"
  name              = "max"
  domain_name       = "tf-max"
  data_disk         = "/kvm/data/max-data"
  memory            = 1024 * 8
  mac               = "CA:FE:C0:FF:EE:03"
}
