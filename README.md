# Max

Max is our VM running all of our web servers, provisioned with Terraform and configured with Ansible.

## Running services

All services below are implemented using Docker:

- Reverse proxy using [Traefik](https://doc.traefik.io/traefik/)
- Git server using [Forgejo](https://forgejo.org/) ([git.pizzapim.nl](https://git.pizzapim.nl))
- Static website using [Jekyll](https://jekyllrb.com/) ([pizzapim.nl](https://pizzapim.nl))
- File sychronisation using [Syncthing](https://syncthing.net/)
- Microblogging server using [Mastodon](https://joinmastodon.org/) ([social.pizzapim.nl](https://social.pizzapim.nl))
- Calendar and contact synchronisation using [Radicale](https://radicale.org/v3.html) ([dav.pizzapim.nl](https://dav.pizzapim.nl))
- KMS server using [vlmcsd](https://github.com/Wind4/vlmcsd)
- Cloud file storage using [Seafile](https://www.seafile.com)
- Disposable mail server using [Inbucket](https://inbucket.org)
- Digital toolbox using [Cyberchef](https://cyberchef.geokunis2.nl)
- Jitsi Meet (https://meet.jit.si)
- RSS feed reader using [FreshRSS](https://miniflux.app/)
- Metrics using [Prometheus](https://prometheus.io/)
- Latex editor using [Overleaf](overleaf.com)
